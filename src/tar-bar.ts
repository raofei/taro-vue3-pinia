export const tabBar = {
  custom: true,
  color: '#000000',
  selectedColor: '#FF0000',
  list: [
    {
      pagePath: 'pages/index/index',
      text: '首页'
    },
    {
      pagePath: 'pages/my/index',
      text: '个人中心'
    }
  ]
};
